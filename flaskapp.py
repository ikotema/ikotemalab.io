from flask import Flask, send_from_directory
from flask.ext.mako import MakoTemplates
from flask.ext.mako import render_template as render_slim
from plim import preprocessor

app = Flask(__name__)
mako = MakoTemplates(app)
app.config.from_pyfile('flaskapp.cfg')
app.config['MAKO_PREPROCESSOR'] = preprocessor

# @app.route('/')
# def index():
#     return render_template('index.html')


@app.route('/')
def hello():
    return render_slim('index.slim', name='mako')


@app.route('/<path:resource>')
def serveStaticResource(resource):
    return send_from_directory('static/', resource)


@app.route("/test")
def test():
    return "<strong>It's Alive!</strong>"

if __name__ == '__main__':
    app.run()
